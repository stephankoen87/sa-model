# Sentiment Analysis

Multilingual sentiment analysis model. The machine learning model predicts whether the sentiment of a text is positive or negative.

The machine learning model was implemented with [Keras](https://keras.io) and trained with 50'000 english, 30'000 german film reviews. The models are tested with [TensorFlow](https://www.tensorflow.org/) as backend.

## Accuracy of the Predictions

The measured accuracy of the predictions is 88.6% for English, 81.3% for German.
